<?php 
class parent_class{

    function __construct(){
        $this->num = 15;
        echo "<br> Parent's class constructor<br>";
    }

    function fun(){
        echo "<br>Hello Manodip";
    }
}

class child_class extends parent_class{

    /*........child class called fifrst .... but we can use parents class into child class...........*/
    function __construct(){
        //parent::__construct();
        echo "Child class constructior";
        $this->num = 35;
        parent::__construct();
       // $this->num = 35;
    }

    function fun(){
        echo "<br> Hello Partho";
        
    }

 

}
$obj = new child_class();
echo $obj->num;
$obj->fun();



?>