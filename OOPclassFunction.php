<?php
class fruit{
    //objectives
    public $name;
    public $color;

    //methods

    function set_name($name){
        $this->name = $name;
    }
    function get_name(){
        return $this->name;
    }
    function set_color($color){
        $this->color = $color;
    }

    function get_color(){
        return $this->color;
    }

}
$Apple = new fruit();
$Banana = new fruit();
$Mango = new fruit();

$Apple->set_name('Apple');
echo $Apple->get_name();

echo "<br>";

$Banana->set_name('Banana');
echo $Banana->get_name();

echo "<br>";
$Apple ->set_color('Green');
echo "Apple's color is: ".$Apple->get_color();

echo "<br>";

$Banana->set_color('yellow');
echo "Banana's color is: ". $Banana->get_color();

?>